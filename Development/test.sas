

* %include "\\home\pardre1\SAS\Scripts\remoteactivate.sas" ;
proc freq order= freq noprint
   data= in_dataset
      (keep= var1)
   ;
   tables var1 / missing out= out_dataset;
run;

compess
abs
ab
as
abs(numeric)

error
warning:
error

data
^<>
dat
data [out_dataset];
   set [in_dataset];
   [Code goes here];
run;

find(string, substring, <, modifiers>, <, startpos>)

mdy(month, day, year)

datepart


datalines;
blah
run;
input(source, <?|??>, informat.)



substr(string, position, <, length>)
find(string, substring, <, modifiers>, <, startpos>)

input

options orientation = landscape ;

* ods graphics / height = 6in width = 10in ;

* %let out_folder = //home/pardre1/ ;
%let out_folder = /C/Documents and Settings/pardre1/Application Data/Sublime Text 2/Packages/SAS/ ;
%let out_folder = C:\Documents and Settings/pardre1/Application Data/Sublime Text 2/Packages/SAS/test.sas ;

ods html path = "&out_folder" (URL=NONE)
         body   = "test.html"
         (title = "test output")
          ;

* ods rtf file = "&out_folder.test.rtf" device = sasemf ;



run ;

ods _all_ close ;
input(source, <?|??>, informat.)

%macro somthing ;
  data gnu ;
    set old ;
  run ;

%mend something ;

data one ;
  set two ;
  if x = 3 then do ;
    slsl ;
  end ;
run ;

cat(item-1, <, ..., item-n>)

&adfl.

data bob ;
  set sashelp.class ;
  if height = 4 then do ;
    weight = 40 ;
  end ;
  if weight = 400 then do ;
    category = 'fatty' ;
  end ;
  else do;
    something ;
  end ;
run ;

data gnu;
  set old;
run;

&blah.



data gnu ;
  set old ;
  if x= 1 then do ;
    y != 4 ;
  end eqeq;eq
  if z = 4 then do ;
    z = 4 ;
  end ;
run ;

  if y = 2 then do ;
    bhal ;
  end ;
<=
>=
<
b <= b

run ;

data three ;
  set four ;
  if x = 12 then do ;
end ;

proc something ;
  if x = 2 then do ;
end ;

%macro x ;
  data bob ;
    set mary ;
    if x = 2 then do ;
  end ;
  run ;
%mend ;



%copy_dataset(muni_Jul, work, cashflowfee_swap_muni, cashflowfee_swap_muni_20140707) ;
%copy_dataset(muni_Jul, work, cashflow_swap_muni, cashflow_swap_muni_20140707) ;
%copy_dataset(muni_Jul, work, tradeleg_swap_muni, tradeleg_swap_muni_20140707) ;
%copy_dataset(muni_Jul, work, trade_swap_muni, trade_swap_muni_20140707) ;

%copy_dataset(muni_Jul, work, cashflowfee_swap_new, cashflowfee_swap_new_20140707) ;
%copy_dataset(muni_Jul, work, cashflow_swap_new, cashflow_swap_new_20140707) ;
%copy_dataset(muni_Jul, work, tradeleg_swap_new, tradeleg_swap_new_20140707) ;
%copy_dataset(muni_Jul, work, trade_swap_new, trade_swap_new_20140707) ;

%copy_dataset(muni_Jul, work, cashflowfee_swap, cashflowfee_swap_20140707) ;
%copy_dataset(muni_Jul, work, cashflow_swap, cashflow_swap_20140707) ;
%copy_dataset(muni_Jul, work, tradeleg_swap, tradeleg_swap_20140707) ;
%copy_dataset(muni_Jul, work, trade_swap, trade_swap_20140707) ;